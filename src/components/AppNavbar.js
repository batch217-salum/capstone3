import { useContext } from "react"
import { NavLink } from "react-router-dom";

import { Container, Nav, Navbar } from "react-bootstrap";
import UserContext from "../UserContext";

export default function AppNavbar(){

    const { user } = useContext(UserContext);
    console.log(user);
    return(
        <Navbar bg="dark" expand="lg" variant="dark" className="dark">
        <Container fluid>
            <Navbar.Brand as={ NavLink } to="/"><img src={require("../images/NoLine.png")}></img></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav>
                {
                    (user.id !== null)
                    ?
                    (user.isAdmin === true)
                        ?
                        <>
                        <Nav.Link as={ NavLink } to="/admin-dashboard" end>Admin Dashboard</Nav.Link>
                        <Nav.Link as={ NavLink } to="/orders" end>Orders</Nav.Link>
                        <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                        </>
                        :
                        <>
                        <Nav.Link as={ NavLink } to="/products" end>Products</Nav.Link>
                        <Nav.Link as={ NavLink } to="/orders" end>Orders</Nav.Link>
                        <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                        </>
                    :
                    <>
                    <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                    <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
                    </>
                }
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}