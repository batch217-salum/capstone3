import {Col, Row, Card} from "react-bootstrap"

export default function Highlights(){
    return(
        <Row className="my-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Shop from Home</h2>
                        </Card.Title>
                        <Card.Text>
                            You can now shop from home using No Line Shop. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Fast Transaction</h2>
                        </Card.Title>
                        <Card.Text>
                            We aim to have faster transaction than shopping onsite.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>No Line Shopping</h2>
                        </Card.Title>
                        <Card.Text>
                            No need to fall in line!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}