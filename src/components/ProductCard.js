import { Card, Button, Modal, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, React, useContext} from 'react';
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function ProductCard({productProp}) {


    const { user } = useContext(UserContext);

    const { _id, name, description, price } = productProp;

    const [viewProduct, setViewProduct] = useState(false)

    const [currentProductId, setCurrentProductId] = useState('')
    const [currentName, setCurrentName] = useState('')
    const [currentPrice, setCurrentPrice] = useState('')
    const [currentDescription, setCurrentDescription] = useState('')
    const [currentQuantity, setCurrentQuantity] = useState(0)

    const [isActive, setIsActive] = useState(false);
    const [isZero, setIsZero] = useState(true);

    const closeModal = () => setViewProduct(false)
    const viewModal = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
        .then(res => res.json())
        .then(data => {
            setCurrentProductId(data._id)
            setCurrentName(data.name)
            setCurrentDescription(data.description)
            setCurrentPrice(data.price)
            setViewProduct(true)
        })
    }

    const orderProduct = () => {

        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                userId: user.id,
                products: {
                    productId: currentProductId,
                    quantity: currentQuantity
                },
                totalAmount: currentQuantity*currentPrice
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                Swal.fire({
                    title: "ORDER SUCCESS!",
                    icon: "success",
                    text: `"The new order was added to the order list.`
                });
                closeModal()
                setCurrentQuantity(0);
            }
            else {
                Swal.fire({
                    title: "ORDER UNSSUCCESSFUL!",
                    icon: "error"
                });
                closeModal()
                setCurrentQuantity(0);
            }
        })
    }
        
    useEffect(() => {

        if(currentQuantity <= 0){
            setIsActive(false)
            setIsZero(true)
        }else{
            setIsActive(true);
            setIsZero(false)
        }

    });


    return (
        <>
            <Card className="my-3">
                <Card.Body>
                    <Card.Title>
                        {name}
                    </Card.Title>
                    <Card.Subtitle>
                        Description:
                    </Card.Subtitle>
                    <Card.Text>
                        {description}
                    </Card.Text>
                    <Card.Subtitle>
                        Price:
                    </Card.Subtitle>
                    <Card.Text>
                        Php {price}
                    </Card.Text>
                    <Button variant="primary" onClick={() => viewModal(_id)}>Buy</Button>
                </Card.Body>
            </Card>

            <Modal show={viewProduct} onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                  <Modal.Title> Product Product</Modal.Title>
                </Modal.Header> 
                <Modal.Body>
                    <Container>
                        <Row className="justify-content-center align-items-center">                            
                            <Col>
                                <div>
                                    <h5>Product Name: {currentName}</h5>
                                    <h5>Price: {currentPrice}</h5>
                                    <h5>Description: {currentDescription}</h5>
                                    <h5>Quantity:<span> </span> 
                                        {
                                            isZero
                                            ?
                                            <Button variant="secondary" onClick={()=>setCurrentQuantity(currentQuantity-1)} disabled>-</Button>
                                            :
                                            <Button variant="secondary" onClick={()=>setCurrentQuantity(currentQuantity-1)}>-</Button>
                                        }
                                        <span>{currentQuantity}</span>
                                        <Button variant="secondary" onClick={()=>setCurrentQuantity(currentQuantity+1)}>+</Button>
                                    </h5>
                                    <h5>Total Amount: <span>{currentQuantity*currentPrice}</span></h5>
                                    
                                </div>
                            </Col>
                        </Row>
                    </Container>

                </Modal.Body>
                <Modal.Footer>
                {
                    (user.id !== null)
                    ?
                    <Button variant="primary" onClick={orderProduct}>
                        Order
                    </Button>
                    :
                    <Button variant="primary" navigate to="/login">
                        Order
                    </Button>

                }
                </Modal.Footer>
              </Modal>
        </>

    )
}