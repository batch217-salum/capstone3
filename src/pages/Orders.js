import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";


export default function Orders() {


	const [orders, setOrders] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/orders/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setOrders(data.map(order =>{
				return(
					<Orders key={order._id} orderProp={order}/>
				);
			}));
		})
	}, []);
	return(
		<>
			<h1>Orders</h1>
			{orders}
		</>
	)
}