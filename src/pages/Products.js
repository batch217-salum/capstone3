import { useEffect, useState } from "react";
import ProductCard from "../components/ProductCard";


export default function Products() {


	const [products, setProducts] = useState([]); 

	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product =>{
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, []);
	return(
		<>
		<div className="w-100 bg-white  rounded shadow-sm shadow-lg p-5 mt-5 mb-5">
            <h1>Products</h1>
            {products}
        </div>
			
			
		</>
	)
}